/*======================================[Protocolos]===========================================================
   Copyright Author Yoel López  <lopez.yoel25@gmail.com>
   All rights reserved.
   Version: 1.0.0
   Creation Date: 2019/04/24
*/
/*===========================[Avoid multiple inclusion - begin]=======================================*/
#ifndef PROTOCOLOS_H
#define PROTOCOLOS_H

/*################## UART ################*/

/*==================================[Definitions of public data types]====================================*/
#define UartSpeed 115200

/*===========================[Prototypes (declarations) of public functions]=======================*/


void initSerial();

void initSerial() {

  Serial.begin(UartSpeed);
  Serial.printf("Conectado a UART a: %d baudios\n", UartSpeed);/*Connect UART*/

}
/*################## WiFi ################*/

/*===========================[Prototypes (declarations) of public functions]=======================*/
void wifiConnect();
void initWiFi();

void wifiConnect() {
  uint8_t retry = 0;
  WiFi.begin("Fibertel WiFi419 2.4GHz", "0043527009");
  Serial.print("Connecting");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
    retry = retry + 1;
    if (retry > 40) {
      while (1) {};
    }
  }
  Serial.println();
  Serial.printf("Conectado a %s con IP %s\n", WiFi.SSID().c_str(), WiFi.localIP().toString().c_str());
}

void initWiFi() {
  WiFi.hostname(ESP_NAME);
  WiFi.mode(WIFI_AP_STA);
  WiFi.setOutputPower(20.5);
  Serial.println("Conectando a WiFi..."); /*Connect station*/
  wifiConnect();
  WiFi.softAP(AP_SSID, AP_PASSWORD); /*Start AP*/
}

/*################## MQTT ################*/

/*==================================[Definitions of public data types]====================================*/
uint32_t times = 1;
uint32_t ack = 0;
uint8_t firmUpdate = 0;
uint64_t sendEntry = 0;

clt_device_t devicelist[MAX_DEVICES];

MQTT myMqtt(ESP_NAME, MQTT_HOST, MQTT_PORT);

/*===========================[Prototypes (declarations) of public functions]=======================*/
void HeaderToMQTT();
void jSonToMQTT();
void mqttConnect();

/*==================================================
    @brief MQTT initialization connection function
    @param  client:   MQTT_Client reference
    @param  host:   Domain or IP string
    @param  port:   Port to connect
    @param  security:   1 for ssl, 0 for none
    @retval None
  ==================================================*/
void initMQTT();
/*==================================================
    @brief  MQTT subsrcibe function.
    @param  client:   MQTT_Client reference
    @param  topic:    string topic will subscribe
    @param  qos:    qos
    @retval TRUE if success queue
  ==================================================*/
void myConnectedCb();
void myDisconnectedCb();

/*==================================================
    @brief  MQTT publish function.
    @param  client:   MQTT_Client reference
    @param  topic:    string topic will publish to
    @param  data:     buffer data send point to
    @param  data_length: length of data
    @param  qos:    qos
    @param  retain:   retain
    @retval TRUE if success queue
  ==================================================*/
void myPublishedCb();
void SuperFakeSniffer();
void myDataCb(String& topic, String& data);


void HeaderToMQTT() {

  String date = getTime();
  String aux =  "{\"Type\":\"Header\",\"i\":" + String(times) + ",\"N\":" + String(N_devices) + ",\"P\":" + String(people) + ",\"R\":" + String(min_rssi_var) + ",\"T\":\"" + date.c_str() + "\",\"heap\":" + ESP.getFreeHeap() + "}";
  if (myMqtt.isConnected()) {

    myMqtt.publish(MQTT_OUT_TOPIC, aux);
  }

  Serial.println("---------HEADER--------");
  Serial.printf("[Header] Veces: %d, Dispositivos: %d, Personas: %d, RSSI: %d, Duración: %s, Heap: %d\n", times, N_devices, people, min_rssi_var, date.c_str(), ESP.getFreeHeap());
  Serial.println("---------HEADER--------");

  ack = 1;
}

void jSonToMQTT() {

  uint16_t i, j, packet, sent, sub;
  uint16_t retry = 0;
  uint32_t memory = 0;

  String JsonBuffer = "";
  String Place = "";
  String MACBuffer = "\"MAC\":[";
  String RSSIBuffer = "\"RSSI\":[";
  String LHTBuffer = "\"LHT\":[";
  String REPBuffer = "\"REP\":[";

  Serial.println("---------DATA--------");

  j = 0;
  packet = 0;
  sent = 0;
  sub = 0;
  for (i = 0; i < MAX_DEVICES; i++)
  {
    ESP.wdtFeed();
    ESP.wdtEnable(0);
    if (devicelist[i].mac != "")
    {
      j++;
      MACBuffer += "\"";
      MACBuffer += (devicelist[i].mac).c_str();
      MACBuffer += "\"";
      RSSIBuffer += devicelist[i].rssi;
      LHTBuffer += ((millis() - devicelist[i].ms) / 1000UL);
      REPBuffer += devicelist[i].reported;

      if (!(j % chop_var))
      {
        packet++;

        MACBuffer += ']';
        RSSIBuffer += ']';
        LHTBuffer += ']';
        REPBuffer += ']';

        Place = "\"i\":" + String(times) + ',' + "\"P\":" + String(packet);

        memory = ESP.getFreeHeap() ;
        JsonBuffer = "{\"Type\":\"Data\"," + Place + ',' + MACBuffer + ',' + RSSIBuffer + ',' + LHTBuffer + ',' + REPBuffer + '}';

        /*Serial.printf("%d - %d  | %d -%d | ", N_devices, packet, memory, ESP.getFreeHeap());*/
        Serial.println(JsonBuffer);

        if (myMqtt.isConnected()) {
          myMqtt.publish(MQTT_OUT_TOPIC, JsonBuffer);
        }
        myMqtt.subscribe(MQTT_OUT_TOPIC);
        sub++;

        JsonBuffer = "";
        MACBuffer = "\"MAC\":[";
        RSSIBuffer = "\"RSSI\":[";
        LHTBuffer = "\"LHT\":[";
        REPBuffer = "\"REP\":[";
        continue;
      }
      if (j < N_devices)
      {
        MACBuffer += ',';
        RSSIBuffer += ',';
        LHTBuffer += ',';
        REPBuffer += ',';
      }
    }
  }

  if (N_devices % chop_var)
  {
    packet++;

    MACBuffer += ']';
    RSSIBuffer += ']';
    LHTBuffer += ']';
    REPBuffer += ']';

    Place = "\"i\":" + String(times) + ',' + "\"P\":" + String(packet);

    memory = ESP.getFreeHeap() ;
    JsonBuffer = "{\"Type\":\"Data\"," + Place + ',' + MACBuffer + ',' + RSSIBuffer + ',' + LHTBuffer + ',' + REPBuffer + '}';

    /*Serial.printf("%d - %d  | %d -%d | ", N_devices, packet, memory, ESP.getFreeHeap());*/
    Serial.println(JsonBuffer);

    Serial.println("---------DATA--------");

    if (myMqtt.isConnected()) {
      myMqtt.publish(MQTT_OUT_TOPIC, JsonBuffer);
    }
    myMqtt.subscribe(MQTT_OUT_TOPIC);
    sub++;
  }

  Serial.printf("%d ACK de %d paquetes\r\n", sub, packet);
  ack += sub;
}

void mqttConnect() {

  Serial.println("MQTT -> Conectando...");
  myMqtt.connect();
  myMqtt.subscribe(MQTT_OUT_TOPIC);
  delay(DELAY_MQTT_CONNECT);
}

void initMQTT() {

  myMqtt.onConnected(myConnectedCb);
  myMqtt.onDisconnected(myDisconnectedCb);
  myMqtt.onPublished(myPublishedCb);
  myMqtt.onData(myDataCb);

  mqttConnect();
}

void myConnectedCb() {
  Serial.println("MQTT > Conectado");
  String aux = "{\"Type\":\"Special\",\"Estado\":\"Conectado\"}";
  boolean result = myMqtt.publish(MQTT_OUT_TOPIC, aux);

  myMqtt.subscribe(MQTT_CONFIG_TOPIC1);
  myMqtt.subscribe(MQTT_CONFIG_TOPIC2);
  myMqtt.subscribe(MQTT_CONFIG_TOPIC3);
  myMqtt.subscribe(MQTT_CONFIG_TOPIC4);
  myMqtt.subscribe(MQTT_CONFIG_TOPIC5);
  myMqtt.subscribe(MQTT_CONFIG_TOPIC6);
}

void myDisconnectedCb() {
  Serial.println("MQTT > Desconectado, reintentando conectar");
  delay(DELAY_MQTT);
  myMqtt.connect();
}

void myPublishedCb() {
  /*Serial.println("Publicado");*/
}

void SuperFakeSniffer() {
  for (int i = 0; i < MAX_DEVICES; i++) {
    devicelist[i].mac = "AA:BB:CC:DD:EE:FF";
    devicelist[i].rssi = -15;
    devicelist[i].ms = millis();
    devicelist[i].reported = 65535;
  }
}

void myDataCb(String& topic, String& data) {

  if (strcmp(topic.c_str(), MQTT_CONFIG_TOPIC6.c_str()) == 0)
  {
    Serial.print(topic);
    Serial.print(" ");
    Serial.println(data);
    String configg = "{\"Type\":\"Special\",\"invalid\":1}";

    switch (data.charAt(0)) {
      case 'f':
        configg = "{\"Type\":\"Special\",\"fake\":1}";
        SuperFakeSniffer();
        break;

      case 'u':
        configg = "{\"Type\":\"Special\",\"firmUpgrade\":1}";
        firmUpdate = 1;
        break;

      case 'r':
        configg = "{\"Type\":\"Special\",\"reset\":1}";
        while (1) {};
        break;

      case 'v':
        configg = "{\"Type\":\"Special\",\"version\":" + VERSION + "}";
        break;

      case 's':
        configg = "{\"Type\":\"Special\",\"rssi\":" + String(min_rssi_var) + "}";
        break;

    }

    if (myMqtt.isConnected()) {
      myMqtt.publish(MQTT_OUT_TOPIC, configg);
    }
  }

  if (strcmp(topic.c_str(), MQTT_CONFIG_TOPIC3.c_str()) == 0)
  {
    Serial.print(topic);
    Serial.print(" ");
    Serial.println(data);
    sendtime_var = data.toInt();
    Serial.printf("Send each:          %d segundos\n", sendtime_var);

    String configg = "{\"Type\":\"Special\",\"sendtime\":" + String(sendtime_var) + '}';
    if (myMqtt.isConnected()) {
      myMqtt.publish(MQTT_OUT_TOPIC, configg);
    }
  }

  if (strcmp(topic.c_str(), MQTT_CONFIG_TOPIC2.c_str()) == 0)
  {
    Serial.print(topic);
    Serial.print(" ");
    Serial.println(data);
    reported_var = data.toInt();
    Serial.printf("Min seen:           %d veces\n", reported_var);

    String configg = "{\"Type\":\"Special\",\"reporte\":" + String(reported_var) + '}';

    if (myMqtt.isConnected()) {
      myMqtt.publish(MQTT_OUT_TOPIC, configg);
    }
  }

  if (strcmp(topic.c_str(), MQTT_CONFIG_TOPIC5.c_str()) == 0)
  {
    Serial.print(topic);
    Serial.print(" ");
    Serial.println(data);
    min_rssi_var = data.toInt();
    Serial.printf("Min RSSI:           %d\n", min_rssi_var);

    String configg = "{\"Type\":\"Special\",\"min_rssi\":" + String(min_rssi_var) + '}';

    if (myMqtt.isConnected()) {
      myMqtt.publish(MQTT_OUT_TOPIC, configg);
    }
  }

  if (strcmp(topic.c_str(), MQTT_CONFIG_TOPIC1.c_str()) == 0)
  {
    Serial.print(topic);
    Serial.print(" ");
    Serial.println(data);
    list_timeout_var = data.toInt();
    Serial.printf("Max time:           %d segundos\n", list_timeout_var);

    String configg = "{\"Type\":\"Special\",\"timeout\":" + String(list_timeout_var) + '}';

    if (myMqtt.isConnected()) {
      myMqtt.publish(MQTT_OUT_TOPIC, configg);
    }

  } else {

    /*Serial.print("ACK: [");
      Serial.print(topic);
      Serial.print("] ");
      Serial.println(data);*/
  }
}

/*=======================[Avoid multiple inclusion - end]======================================*/
#endif //PROTOCOLOS_H
