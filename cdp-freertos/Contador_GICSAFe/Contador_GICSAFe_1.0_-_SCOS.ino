/*============================================[Main]====================================================================================
   Copyright Author Yoel López  <lopez.yoel25@gmail.com>
   All rights reserved.
   Version: 1.0.0
   Creation Date: 2019/06/15
*/

/*======================================[Inclusions of function dependencies]===========================================================*/
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <set>

/*======================================[Inclusions of function dependencies - Cooperative OS]===========================================================*/
#include "SCOS.h"

WiFiClient wifiClient;
void inline handler (void);

/*======================================[Initialize OS Schedule]===========================================================*/
void setup()
{
  initOS();
}

/*======================================[Schedule Tasks]===========================================================*/
void loop()
{
  dispatchTasks();
}
