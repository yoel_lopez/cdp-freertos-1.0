/*======================================[Configuracion]===========================================================
   Copyright Author Yoel López  <lopez.yoel25@gmail.com>
   All rights reserved.
   Version: 1.0.0
   Creation Date: 2019/04/24
*/
/*===========================[Avoid multiple inclusion - begin]=======================================*/
#ifndef CONFIGURACION_H
#define CONFIGURACION_H

#include <String.h>

/*################## Informacion ################*/

static String VERSION = "1.4.1b";

/*################## Trenes #####################*/

/*==================================[Definitions of public data types]====================================*/
#define L0 "Invalido"
#define L1 "Roca"
#define L2 "San_Martin"
#define L3 "Sarmiento"
#define L4 "Mitre"
#define L5 "Belgrano_Sur"

typedef enum {LINEA0, LINEA1, LINEA2, LINEA3, LINEA4, LINEA5} t_lines;

static const char* lines[] = {L0, L1, L2, L3, L4, L5};

/*################## Configuracion ################*/

/*==================================[Definitions of public data types]====================================*/

ADC_MODE(ADC_VCC);                                        // Pin a sensar (Conectar A0 con 3V para medir 3.3V)
#define SERIAL_SET_DEBUG_OUTPUT  false                   // Activar modo debug
#define CHOP 25                                        // Cantidad de elementos en la trama de datos
#define LINE LINEA3                                   // Linea de Trenes Argentinos (ver Trenes)
#define TRAIN 8                                      // Numero de formacion
#define CAR 6                                       // Numero de coche (1a7 o 1a9 segun linea)
#define TXPOWER -20.5

static const uint16_t MAX_DEVICES = 250;      // Maxima cantidad de dispositivos a almacenar
static uint16_t N_devices;
uint16_t people;
uint16_t list_timeout_var = 150;
uint16_t reported_var = 3;
uint16_t sendtime_var = 1 * 60;
static uint8_t chop_var = 25;
int8_t min_rssi_var = -74;
static String URL_UPDATE = "http://materias.fi.uba.ar/6667/varios/CdP" + String(LINE) + String(TRAIN) + String(CAR) + ".bin";

/* ####################### Interrupciones ######################## */

/*==================================[Definitions of public data types]====================================*/

#define timer0_preload 40161290/500
#define my_delay 100

/*################## WiFi ################*/

/*==================================[Definitions of public data types]====================================*/

static String aux = "GICSAFe" + String(LINE) + String(TRAIN) + String(CAR); // Host name
static const char *ESP_NAME = aux.c_str();                // Host name
static const char *AP_SSID = aux.c_str();                // Nombre del AP
static const char *AP_PASSWORD = "GICSAFeMOS";          // Contraseña del AP
static const ap_t AP_LIST[] = {                        // Lista de conexiones a probar
  {"Fibertel WiFi419", "0043527009"}
};

/*################## MQTT ################*/

/*==================================[Definitions of public data types]====================================*/

#define MQTT_PORT  1883                                                  // Puerto del broker

static const char* MQTT_HOST = "gicsafe.sofse.gob.ar";                  // Direccion del broker GTI TRENES

static String Address = "/cdp/" + String(lines[LINE]) + "/F_" + TRAIN + "/C_" + CAR + "/";
static String MQTT_OUT_TOPIC = Address;                                // Topico del broker
static String MQTT_CONFIG_TOPIC1 = "/cdp/configout/timeout" + String(LINE) + String(TRAIN) + String(CAR) + "/";
static String MQTT_CONFIG_TOPIC2 = "/cdp/configout/reported" + String(LINE) + String(TRAIN) + String(CAR) + "/";
static String MQTT_CONFIG_TOPIC3 = "/cdp/configout/sendtime" + String(LINE) + String(TRAIN) + String(CAR) + "/";
static String MQTT_CONFIG_TOPIC4 = "/cdp/configout/chop" + String(LINE) + String(TRAIN) + String(CAR) + "/";
static String MQTT_CONFIG_TOPIC5 = "/cdp/configout/rssi" + String(LINE) + String(TRAIN) + String(CAR) + "/";
static String MQTT_CONFIG_TOPIC6 = "/cdp/configout/config" + String(LINE) + String(TRAIN) + String(CAR) + "/";

/*################## Timer ################*/

/*==================================[Definitions of public data types]====================================*/

#define day  86400000     // 86400000 milisegundos por dia 
#define hour  3600000    // 3600000 milisegundos en una hora
#define minute  60000   // 60000 millisegundos en un minuto
#define second   1000  // 1000 millisegundos en un segundo

/*################## Delays ################*/

/*==================================[Definitions of public data types]====================================*/

#define DELAY_BEACON        500
#define DELAY_MQTT          1000 //500
#define DELAY_MQTT_CONNECT  1000 //10
#define DELAY_PRINT         1
#define DELAY_RX            5

/*=======================[Avoid multiple inclusion - end]======================================*/
#endif //CONFIGURACION_H
