/*======================================[Interrupciones]===========================================================
   Copyright Author Yoel López  <lopez.yoel25@gmail.com>
   All rights reserved.
   Version: 1.0.0
   Creation Date: 2019/04/24
*/
/*===========================[Avoid multiple inclusion - begin]=======================================*/
#ifndef INTERRUPCIONES_H
#define INTERRUPCIONES_H

/*################## HANDLER - BEACONS ################*/

/*==================================[Definitions of public data types]====================================*/
#define TAMANIO_HEAP 2500
WiFiEventHandler probeRequestHandler;

/*===========================[Prototypes (declarations) of public functions]=======================*/
void beaconDetect(void);

void inline handler (void) {

  uint32_t heap = ESP.getFreeHeap();
  if (heap < TAMANIO_HEAP)
  {
    Serial.printf("<%u>\r\n", ESP.getFreeHeap());
    Serial.println("Cantidad de heap critico! desconectando MQTT");
  }
  timer0_write(ESP.getCycleCount() + timer0_preload * my_delay);
}

void onProbeRequest(const WiFiEventSoftAPModeProbeRequestReceived &evt) {

  String mac = macToString(evt.mac);

  if (evt.rssi <= min_rssi_var) // Filtro de potencia
  {
    /*Serial.printf("%d,%d\r\n", evt.rssi, min_rssi_var);*/
    return;
  }

  if (!strncmp(mac.c_str(), "da:a1:19", 8)) // Filtro de MAC aleatoria
  {
    Serial.println("Random mac!");
    return;
  }

  if (!strncmp(mac.c_str(), "0c:cb:85", 8)) // Filtro de MAC aleatoria
  {
    Serial.println("Random mac!");
    return;
  }
  uint16_t i;
  for (i = 0; i < MAX_DEVICES; i++) {   // Actualizando MACs ya ingresadas
    if (devicelist[i].mac == mac) {
      // Update device
      devicelist[i].rssi = evt.rssi;
      devicelist[i].ms = millis();
      devicelist[i].reported++;
      return;
    }
  }

  for (i = 0; i < MAX_DEVICES; i++) {   // Agregando MAC nueva
    if (devicelist[i].mac == "") {

      //Add device
      devicelist[i] = {.mac = mac, .rssi = (int8_t)evt.rssi, .ms = millis(), .reported = 0};

      /*Serial.printf("[Device In] MAC: %s, RSSI: %d, LHT: %u Reported: %d\n", mac.c_str(), evt.rssi, millis(), devicelist[i].reported);*/
      return;
    }
  }
  /*Serial.println("No se pudo agregar a la lista");*/
}

void beaconDetect(void) {
  probeRequestHandler = WiFi.onSoftAPModeProbeRequestReceived(&onProbeRequest); // Handler de probe request
  timer0_isr_init();
  timer0_attachInterrupt(handler);
  timer0_write(ESP.getCycleCount() + timer0_preload * my_delay);
  interrupts();
}

/*=======================[Avoid multiple inclusion - end]======================================*/
#endif //INTERRUPCIONES_H
