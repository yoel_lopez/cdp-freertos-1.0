/*======================================[SCOS]===========================================================
   Copyright Author Yoel López  <lopez.yoel25@gmail.com>
   All rights reserved.
   Version: 1.0.0
   Creation Date: 2019/06/09
*/
/*===========================[Avoid multiple inclusion - begin]=======================================*/
#ifndef SCOS_H
#define SCOS_H

#include <MQTT.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>
#include "Estructuras.h"
#include "Configuracion.h"
#include "ConversionDatos.h"
#include "MenuMEF.h"
#include "Interrupciones.h"

#include <TaskScheduler.h>

/*==================================[Definitions of public data types]========================================*/
#define INTERVAL_CONNECT      5000
#define INTERVAL_FSM_INIT     1000
#define INTERVAL_FSM_PROCESS  5000
/*==================================[Internal Data Definitions]==============================================*/

/*===========================[Prototypes (declarations) of external public functions]=======================*/
/*Callback methods prototypes*/

void initConnectionsCallback();
void initFSMCallback();
void menuMefProcessCallback();

Task initConnections(INTERVAL_CONNECT, TASK_ONCE, &initConnectionsCallback);
Task initFSM(INTERVAL_FSM_INIT, TASK_ONCE, &initFSMCallback);
Task fsmProcess(INTERVAL_FSM_PROCESS, TASK_FOREVER, &menuMefProcessCallback);

Scheduler runner;

/*===========================[Prototypes (declarations) of internal public functions]=======================*/

void initConnectionsCallback() {    /* Initialize Connections */

  initSerial();            /*Initialize UART connection */
  initWiFi();             /* Initialize WIFI connection */
  initMQTT();            /*  Initialize MQTT connection */
  beaconDetect();

  Serial.println(F("initialized Connections"));
}

void initFSMCallback() {     /* Initialize FSM */

  Serial.println(F("initialize FSM"));
  menuMefInitialize();  /*   Initialize FSM */
}

void menuMefProcessCallback() { /* Upate FSM */
  Serial.println(F("update FSM"));
  menuMefProcess();
}

void taskInit(void) {     /* Add and Enable Tasks */

  runner.addTask(initConnections);
  Serial.println(F("added initConnections"));
  runner.addTask(initFSM);
  Serial.println(F("added initFSM"));
  runner.addTask(fsmProcess);
  Serial.println(F("added fsmProcess"));

  //delay(5000);

  initConnections.enable();
  Serial.println(F("Enabled Task initConnections"));
  initFSM.enable();
  Serial.println(F("Enabled Task initFSM"));
  fsmProcess.enable();
  Serial.println(F("Enabled Task menuMefProcess"));

}

/*==================[Prototypes (declarations) of external public functions]==============================*/

void initOS(void) { /* Initialize OS configurations and variables */

  runner.init(); /* Initialize Scheduler */
  taskInit();
}
void dispatchTasks(void) { /* Dispatch tasks according to scheduler instructions */

  runner.execute();
}
/*=======================[Avoid multiple inclusion - end]======================================*/
#endif //SCOS_H
