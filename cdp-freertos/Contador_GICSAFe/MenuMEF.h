/*======================================[MEF]===========================================================
   Copyright Author Yoel López  <lopez.yoel25@gmail.com>
   All rights reserved.
   Version: 1.0.0
   Creation Date: 2019/04/24
*/
/*===========================[Avoid multiple inclusion - begin]=======================================*/
#ifndef MENU_MEF_H
#define MENU_MEF_H

#include "Protocolos.h"
#include "Pasajeros.h"
#include "OTA.h"

/*==================================[Definitions of public data types]====================================*/

/* FSM State Names */

typedef enum {Configuracion, Operacion} fsmState_t;

fsmState_t menu;

/*===========================[ FSM Prototypes (declarations) of public functions]=======================*/

/*==================================================
    @brief  Initializes FSM in a default state
    @param[in]: does not apply
    @return: does not apply
  ==================================================*/
void menuMefInitialize();

/*==================================================================================================================================================================
    @brief  t allows the finite state machine to work in two modes:
    Query mode in which it obtains information about the device and the firmware version; and in Debug Mode where you get information about the processed data frames.
    @param[in]:does not apply
    @return: does not apply
  ==================================================================================================================================================================*/
void menuMefProcess();

/*===========================[ FSM Implementations]=======================*/
void menuMefInitialize()
{
  Serial.println("********** Bienvenido a la aplicación Contador de Pasajeros - Trenes Argentinos -***********");
  menu = Operacion ;
  Serial.println("********************************************************************************************");
}

void menuMefProcess() {

  char cadena[10];    /*Char Array send to console*/
  byte posicion = 0; /*Char position in Array*/
  static int enteredValue;
  int flag_buffer = 0 ;
  switch (menu) {

    case Configuracion:
      {
        String resetInfo = ESP.getResetReason();
        Serial.setDebugOutput(SERIAL_SET_DEBUG_OUTPUT);
        Serial.printf("\n\n     %s v%s\n\n", ESP_NAME, VERSION.c_str());
        Serial.println("********** Crashes ***********");
        Serial.printf("Reset Reason:       %s\n", resetInfo.c_str());

        Serial.println("********** Firmware ***********");

        Serial.printf("STA MAC:            %s\n", WiFi.macAddress().c_str());
        Serial.printf("AP MAC:             %s\n", WiFi.softAPmacAddress().c_str());
        Serial.printf("Chip ID:            %6X\n", ESP.getChipId());
        Serial.printf("Sketch size:        %s\n", prettyBytes(ESP.getSketchSize()).c_str());
        Serial.printf("Free Sketch size:   %s\n", prettyBytes(ESP.getFreeSketchSpace()).c_str());
        Serial.printf("Chip size:          %s\n", prettyBytes(ESP.getFlashChipRealSize()).c_str());
        Serial.printf("Heap size:          %s\n", prettyBytes(ESP.getFreeHeap()).c_str());
        Serial.printf("SDK version:        %s\n", ESP.getSdkVersion());
        Serial.printf("CPU frequency:      %d MHz\n", ESP.getCpuFreqMHz());
        Serial.printf("Boot Mode           %u\n", ESP.getBootMode());
        Serial.printf("Boot Version:       %u\n", ESP.getBootVersion());

        Serial.println("******** Configuration *********");
        Serial.printf("Max devices:        %d\n", MAX_DEVICES);
        Serial.printf("Packet size:        %d\n", chop_var);
        Serial.printf("Min RSSI:           %d\n", min_rssi_var);
        Serial.printf("Max time:           %d segundos\n", list_timeout_var);
        Serial.printf("Min seen:           %d veces\n", reported_var);
        Serial.printf("Send each:          %d segundos\n", sendtime_var);

        Serial.println("************ Train  ************");
        Serial.printf("Line:               %s\n", String(lines[LINE]).c_str());
        Serial.printf("Formation:          %d\n", TRAIN);
        Serial.printf("Car:                %d\n", CAR);
        Serial.println("*******************************");

        if (Serial.available() > 0) {
          memset(cadena, 0, sizeof(cadena));
          while (Serial.available() > 0)        /*While the buffer has data -> Execute the function */
          {
            delay(DELAY_RX);
            cadena[posicion] = Serial.read();
            posicion++;
          }

          enteredValue = atoi(cadena);         //Convert string to int

          if (enteredValue == 1)
          {
            menu = Configuracion;
          }
        }
        posicion = 0;                      //Set posicion to 0 again
      }
      break;

    case Operacion:
      {
        if (WiFi.status() != WL_CONNECTED) /*Handle WIFI connection*/
        {
          wifiConnect();
          return;
        }

        if (!myMqtt.isConnected()) /*Handle MQTT connection*/
        {
          myMqtt.connect();
          delay(DELAY_MQTT);
          return;
        }
        checkList();           /*Handle devices list*/
        calculatePeople();    /*Calculate passengers */
        actualizarFirmware();

        if (millis() - sendEntry > sendtime_var * MULTIPLICADOR)
        {
          flag_buffer = 1;
          sendEntry = millis();
          if (flag_buffer = 1) {
            HeaderToMQTT();
            jSonToMQTT();
            flag_buffer = 0;
          }
          times++;
        }
        if (Serial.available() > 0) {
          memset(cadena, 0, sizeof(cadena));
          while (Serial.available() > 0)        /*While the buffer has data -> Execute the function */
          {
            delay(DELAY_RX);
            cadena[posicion] = Serial.read();
            posicion++;
          }

          enteredValue = atoi(cadena);         //Convert string to int

          if (enteredValue == 2)
          {
            menu = Operacion;
          }

        }
        posicion = 0;                      //Set posicion to 0 again
      }
      break;

    default:
      menuMefInitialize();
      break;
  }
}

/*=======================[Avoid multiple inclusion - end]======================================*/
#endif //MENU_MEF_H
