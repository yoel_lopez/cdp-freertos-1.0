/*======================================[Estructuras]===========================================================
   Copyright Author Yoel López  <lopez.yoel25@gmail.com>
   All rights reserved.
   Version: 1.0.0
   Creation Date: 2019/04/24
*/
/*===========================[Avoid multiple inclusion - begin]=======================================*/
#ifndef ESPRESENCE_ESPRESENCE_H
#define ESPRESENCE_ESPRESENCE_H

/*==================================[Definitions of public data types]====================================*/

typedef struct {
  const char *ssid;
  const char *pwd;
} ap_t;

typedef struct {
  String mac;
  int8_t rssi;         // -128 a 127
  uint32_t ms;         // 0 a 4,294,967,295
  uint16_t reported;   // 0 a 65535
} clt_device_t;

/*=======================[Avoid multiple inclusion - end]======================================*/
#endif //ESPRESENCE_ESPRESENCE_H
