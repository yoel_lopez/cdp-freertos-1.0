/*======================================[OTA - Actualización de Firmware]===========================================================
   Copyright Author Yoel López  <lopez.yoel25@gmail.com>
   All rights reserved.
   Version: 1.0.0
   Creation Date: 2019/04/24
*/
/*===========================[Avoid multiple inclusion - begin]=======================================*/
#ifndef OTA_H
#define OTA_H

/*===========================[Prototypes (declarations) of public functions]=======================*/

void actualizarFirmware(void);

void actualizarFirmware(void)
{
  if (firmUpdate == 1) {
    t_httpUpdate_return ret = ESPhttpUpdate.update(URL_UPDATE);
    switch (ret) {
      case HTTP_UPDATE_FAILED:
        Serial.printf("HTTP_UPDATE_FAILD Error (%d): %s", ESPhttpUpdate.getLastError(), ESPhttpUpdate.getLastErrorString().c_str());
        break;

      case HTTP_UPDATE_NO_UPDATES:
        Serial.println("HTTP_UPDATE_NO_UPDATES");
        break;

      case HTTP_UPDATE_OK:
        Serial.println("HTTP_UPDATE_OK");
        break;
    }
  }
}

/*=======================[Avoid multiple inclusion - end]======================================*/
#endif //OTA_H
