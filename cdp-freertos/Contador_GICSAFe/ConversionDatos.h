/*======================================[Conversion Datos]===========================================================
   Copyright Author Yoel López  <lopez.yoel25@gmail.com>
   All rights reserved.
   Version: 1.0.0
   Creation Date: 2019/04/24
*/
/*===========================[Avoid multiple inclusion - begin]=======================================*/
#ifndef CONVERSION_DATOS_H
#define CONVERSION_DATOS_H

/*==================================[Definitions of public data types]====================================*/

#define MULTIPLICADOR 1000

/*===========================[Prototypes (declarations) of public functions]=======================*/
String getTime();
uint32_t getUptimeSecs();
String prettyBytes(uint32_t bytes);
String macToString(const uint8 mac[6]);

String getTime() {
  String date = "";
  uint32_t timeNow = millis();

  uint16_t days = timeNow / day ;
  uint8_t hours = (timeNow % day) / hour;
  uint8_t minutes = ((timeNow % day) % hour) / minute ;
  uint8_t seconds = (((timeNow % day) % hour) % minute) / second;

  date += (days < 10) ? ('0' + String(days)) : (String(days));
  date += ':';
  date += (hours < 10) ? ('0' + String(hours)) : (String(hours));
  date += ':';
  date += (minutes < 10) ? ('0' + String(minutes)) : (String(minutes));
  date += ':';
  date += (seconds < 10) ? ('0' + String(seconds)) : (String(seconds));

  return date;
}

uint32_t getUptimeSecs() {
  static uint32_t uptime = 0;
  static uint32_t previousMillis = 0;
  uint32_t now = millis();

  uptime += (now - previousMillis) / 1000UL;
  previousMillis = now;
  return uptime;
}

String prettyBytes(uint32_t bytes) {

  const char *suffixes[7] = {"B", "KB", "MB", "GB", "TB", "PB", "EB"};
  uint8_t s = 0;
  double count = bytes;

  while (count >= 1024 && s < 7) {
    s++;
    count /= 1024;
  }
  if (count - floor(count) == 0.0) {
    return String((int) count) + suffixes[s];
  } else {
    return String(round(count * 10.0) / 10.0, 1) + suffixes[s];
  };
}

String macToString(const uint8 mac[6]) {
  char buf[20];
  snprintf(buf, sizeof(buf), "%02x:%02x:%02x:%02x:%02x:%02x", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
  return String(buf);
}

/*=======================[Avoid multiple inclusion - end]======================================*/
#endif //CONVERSION_DATOS_H
