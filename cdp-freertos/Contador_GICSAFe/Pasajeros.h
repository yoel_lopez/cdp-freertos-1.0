/*======================================[Pasajeros]===========================================================
   Copyright Author Yoel López  <lopez.yoel25@gmail.com>
   All rights reserved.
   Version: 1.0.0
   Creation Date: 2019/04/24
*/
/*===========================[Avoid multiple inclusion - begin]=======================================*/
#ifndef PASAJEROS_H
#define PASAJEROS_H
/*################## PASAJEROS ################*/

/*===========================[Prototypes (declarations) of public functions]=======================*/
void printlist();
void checkList();
void calculatePeople();

void printlist() {

  uint16_t i;
  String date = getTime();

  /*Serial.printf("\r\nDispositivos: %d | Personas: %d | Tension: %.2f V (Funcionando hace %s)\r\n", N_devices, people, (float)ESP.getVcc() / 1024.0, date.c_str());*/

  if (N_devices > 0) {                  // List devices
    for (i = 0; i < MAX_DEVICES; i++) {
      ESP.wdtFeed();
      ESP.wdtEnable(0);
      if (devicelist[i].mac != "") {
        /*Serial.printf(
          "MAC: %s, RSSI: %d, Reported: %d, Visto hace %u segundos\r\n",
          (devicelist[i].mac).c_str(),
          devicelist[i].rssi,
          devicelist[i].reported,
          (unsigned int)((millis() - devicelist[i].ms) / 1000UL)
          );*/
        delay(DELAY_PRINT);
      }
    }
  }
  Serial.println();
}

void checkList() {

  uint16_t i;
  for (i = 0; i < MAX_DEVICES; i++) {
    if (devicelist[i].mac != "" && (millis() - devicelist[i].ms > list_timeout_var * MULTIPLICADOR)) {

      /*Serial.printf("[Device Out] MAC: %s\n", (devicelist[i].mac).c_str());*/ // Clear expired device
      devicelist[i] = {.mac = "", .rssi = 0, .ms = 0, .reported = 0};

    }
  }
  char c;
  while (Serial.available() > 0) // Check console and print list if \n char is detected
  {
    c = Serial.read();
    if (c == '\n') {
      printlist();
    }
  }
}

void calculatePeople() {
  uint16_t i;

  N_devices = 0;
  people = 0;

  // Count devices
  for (i = 0; i < MAX_DEVICES; i++) {
    if ((devicelist[i].mac != "") && (devicelist[i].rssi != 0)) {
      N_devices++;
      if (devicelist[i].reported > reported_var)
        people++;
    }
  }
}

/*=======================[Avoid multiple inclusion - end]======================================*/
#endif //PASAJEROS_H
